package pl.oakfusion.forge.test;

import pl.oakfusion.data.message.Command;
import pl.oakfusion.data.message.Event;

import java.time.LocalDateTime;

public class TestMessages {
    public static class MockCommand extends Command {

        public MockCommand() {
            super(LocalDateTime.now());
        }
    }
    public static class MockEvent extends Event {

        public MockEvent() {
            super(LocalDateTime.now());
        }
    }

    public static class MockEvent2 extends Event {

        public MockEvent2() {
            super(LocalDateTime.now());
        }
    }
}
