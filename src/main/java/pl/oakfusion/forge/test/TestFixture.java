package pl.oakfusion.forge.test;

import pl.oakfusion.bus.MessageBus;
import pl.oakfusion.data.message.Command;
import pl.oakfusion.data.message.Error;
import pl.oakfusion.data.message.Event;
import pl.oakfusion.data.message.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.fail;

public class TestFixture {

	private final MessageBus messageBus;
	private Runnable given;
	private Runnable when;
	private final List<Message> capturedMessages = new ArrayList<>();
	private final TestContext testContext;
	private int currentCapturedMessageIndex;

	public TestFixture(MessageBus messageBus) {
		this.messageBus = messageBus;
		testContext = new TestContext(messageBus);
	}

	private void clear() {
		capturedMessages.clear();
		currentCapturedMessageIndex = 0;
	}

	public TestFixture given(Runnable given) {
		clear();
		this.given = given;
		return this;
	}

	public TestFixture when(Runnable when) {
		this.when = when;
		return this;
	}

	public <C extends Command> TestFixture whenPostingCommand(C command) {
		testContext.postingMessage(command);
		return this;
	}

	public <E extends Event> TestFixture whenPostingEvent(E event) {
		testContext.postingMessage(event);
		return this;
	}

	public void then(Runnable then) {
		messageBus.registerMessageHandler(Message.class, capturedMessages::add);
		checkPreconditions();
		testContext.performTest();
		then.run();
	}

	public <M extends Message> void thenOnMessage(Class<M> clazz, Consumer<M> handler) {
		onMessage(clazz, handler);
	}

	public <C extends Command> void thenOnCommand(Class<C> clazz, Consumer<C> handler) {
		onMessage(clazz, handler);
	}

	public <E extends Event> void thenOnEvent(Class<E> clazz, Consumer<E> handler) {
		onMessage(clazz, handler);
	}

	public <E extends Error> void thenOnError(Class<E> clazz, Consumer<E> handler) {
		onMessage(clazz, handler);
	}

	private <M extends Message> void onMessage(Class<M> clazz, Consumer<M> handler) {
		checkPreconditions();
		boolean handlerPerformed = testContext.received(clazz, message -> {
			capturedMessages.add(message);
			handler.accept(message);
		});
		if (!handlerPerformed) {
			fail("message handler has not been called");
		}
	}

	public List<Message> getCapturedMessages() {
		return capturedMessages;
	}

	private void checkPreconditions() {
		given.run();
		if (when != null) {
			when.run();
		}
	}

	@SuppressWarnings("unchecked")
	public <C extends Command> C expectCommand(Class<C> commandClass) {
		return (C) expectMessage(commandClass, "expected command [%s] has not been sent");
	}

	@SuppressWarnings("unchecked")
	public <E extends Event> E expectEvent(Class<E> eventClass) {
		return (E) expectMessage(eventClass, "expected event [%s] has not been sent");
	}

	@SuppressWarnings("unchecked")
	public <E extends Error> E expectError(Class<E> errorClass) {
		return (E) expectMessage(errorClass, "expected error [%s] has not been sent");
	}

	@SuppressWarnings("unchecked")
	public <M extends Message> M expectMessage(Class<M> messageClass) {
		return (M) expectMessage(messageClass, "expected message [%s] has not been sent");
	}

	private <M extends Message> Message expectMessage(Class<M> messageClass, String description) {
		assertThat(capturedMessages.size())
				.describedAs(description, messageClass.getName())
				.isGreaterThan(currentCapturedMessageIndex);
		Message message = this.nextMessage();
		assertThat(message).isInstanceOf(messageClass);
		return message;
	}

	@SuppressWarnings("unchecked")
	private <M extends Message> M nextMessage() {
		assertThat(capturedMessages.size()).describedAs("no more messages available").isGreaterThan(currentCapturedMessageIndex);
		return (M) capturedMessages.get(currentCapturedMessageIndex++);
	}

}
